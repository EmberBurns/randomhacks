#!/bin/bash

# Halt the script on any errors.
set -e

# Destination Folder on External Storage
target_path="/Volumes/Storage/From_Dunebuggy"

# A list of absolute paths to backup. 
include_paths=(
  "${HOME}/.ssh"
  "${HOME}/.bash_history"
  "${HOME}/.bashrc"
  "${HOME}/.gitconfig"
  "${HOME}/.gitignore"
  "${HOME}/.zshrc"
  "${HOME}/Artifacts"
  "${HOME}/Documents"
  "${HOME}/Downloads"
  "${HOME}/Pictures"
  "${HOME}/Movies"
)

# A list of folder names and files to exclude.
exclude_paths=(
  ".bundle"
  "node_modules"
  "tmp"
)

# Passing list of paths to exclude
for item in "${exclude_paths[@]}"
do
  exclude_flags="${exclude_flags} --exclude=${item}"
done

# Passing list of paths to copy
for item in "${include_paths[@]}"
do
  include_args="${include_args} ${item}"
done

# Finally, rsync with a few flags:
#  -a is archive mode 
#  -v is verbose mode
#  -R is relative mode
# Executing the script only if the External Volume is available

LOCALMOUNTPOINT="/Volumes/Storage"
if mount | grep "on $LOCALMOUNTPOINT" > /dev/null; then
    echo "Storage is mounted"
    echo "Starting RSync Copy now..."
    rsync -avR ${exclude_flags} ${include_args} ${target_path}
else
    echo "Storage is not mounted"
    echo "Can not do RSync Copy"
fi
